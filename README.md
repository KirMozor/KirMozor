## I am a Python web developer and C# developer

### Language and Tools
![Flutter](https://img.shields.io/badge/-Python-edf1f4?style=for-the-badge&logo=python)
![Flutter](https://img.shields.io/badge/-C%23-edf1f4?style=for-the-badge&logo=csharp&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-.NET-edf1f4?style=for-the-badge&logo=dotnet&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-Telegram-edf1f4?style=for-the-badge&logo=telegram&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-Unity-edf1f4?style=for-the-badge&logo=unity&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-Linux-edf1f4?style=for-the-badge&logo=linux&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-Blender-edf1f4?style=for-the-badge&logo=blender&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-SQL-edf1f4?style=for-the-badge&logo=mysql&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-Qt-edf1f4?style=for-the-badge&logo=qt&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-Gtk-edf1f4?style=for-the-badge&logo=gtk&logoColor=3776ab)
![Flutter](https://img.shields.io/badge/-ArchLinux-edf1f4?style=for-the-badge&logo=archlinux&logoColor=3776ab)

### E-mail me
[![Flutter](https://img.shields.io/badge/-Telegram-edf1f4?style=for-the-badge&logo=telegram&logoColor=3776ab)](https://t.me/KirMozorr)

### My channels
[![Flutter](https://img.shields.io/badge/-Telegram-edf1f4?style=for-the-badge&logo=telegram&logoColor=3776ab)](https://t.me/KirMozor)
[![Flutter](https://img.shields.io/badge/-Mastodon-edf1f4?style=for-the-badge&logo=mastodon&logoColor=3776ab)](https://mastodon.social/@kirmozor)

Hello, I am a Python and C# developer, I can write bots for Telegram and Discord. I have worked with MySQL, Mariadb and SQLite. I am also learning a little bit about Unity and C#. I can also analyze web pages, this is proven by the JUT-DL project. I can write GUI interfaces (GTK3). I have worked with Api YandexMusic

---

Not so long ago I wrote [API for YandexMusic on C#](https://gitlab.com/KirMozor/YandexMusicApi).

---

And also I write a [client for Yandex music for UNIX](https://gitlab.com/KirMozor/Yamux) (in general it is a cross-platform) where my API is used.

---

You can see my projects below.
